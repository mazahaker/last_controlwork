Rails.application.routes.draw do
  root 'establishments#index'
  ActiveAdmin.routes(self)
  resources :categories, only: :show
  devise_for :users
  resources :establishments, except: :destroy do
    resources :reviews, only: [:create, :destroy]
    resources :images, only: [:create]
  end
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
