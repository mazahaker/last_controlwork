class CreateReviews < ActiveRecord::Migration[5.1]
  def change
    create_table :reviews do |t|
      t.text :body
      t.integer :quality_food
      t.integer :service_quality
      t.integer :interior
      t.references :user, foreign_key: true
      t.references :establishment, foreign_key: true

      t.timestamps
    end
  end
end
