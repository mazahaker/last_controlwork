class CreateUsers < ActiveRecord::Migration[5.1]
  def change
    create_table :users do |t|
      t.string :username
      t.boolean :active_status, default: false
      t.boolean :admin, default: false


      t.timestamps
    end
  end
end
