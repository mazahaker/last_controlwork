admin = User.create!(username: 'admin', email: 'admin@admin.ru', password: 'qweqweqwe', admin: true, active_status: true)

10.times do
  user = User.create!(username: 'user', email: Faker::Internet.unique.email, password: 'qweqweqwe', active_status: Faker::Boolean.boolean, admin: false)
end

5.times do
  category = Category.create!(title: Faker::Color.unique.color_name)
end

30.times do
  establishment = Establishment.create!(title: Faker::Company.unique.name,
                  description: Faker::Company.catch_phrase,
                  user_id: Faker::Number.between(1, 11),
                  active_status: Faker::Boolean.boolean,
                  category_id: Faker::Number.between(1, 5),
                  agreement: true,
                  picture: File.new("#{Rails.root}/app/assets/images/1.jpg"))
  3.times do
    image = Image.create!(user_id: Faker::Number.between(1, 11),
                          establishment_id: establishment.id,
                          image: File.new("#{Rails.root}/app/assets/images/#{rand(2..3)}.jpg"))
  end
end

30.times do
  review = Review.create!(body: Faker::Book.title,
                        quality_food: Faker::Number.between(1, 5),
                        service_quality: Faker::Number.between(1, 5),
                        interior: Faker::Number.between(1, 5),
                        user_id: Faker::Number.between(1, 11),
                        establishment_id: Faker::Number.between(1, 30))
end
