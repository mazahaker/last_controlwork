ActiveAdmin.register Establishment do
# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
permit_params :picture, :title, :active_status, :description, :user_id
# permit_params :list, :of, :attributes, :on, :model
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if params[:action] == 'create' && current_user.admin?
#   permitted
# end
  index do
    selectable_column
    id_column
    column :picture do |establishment|
      image_tag establishment.picture.url(:normal)
    end
    column :title do |establishment|
      link_to establishment.title, admin_establishment_path(establishment)
    end
    column :active_status
    column :user
    actions
  end

  show do
    render 'show', { establishment: establishment }
  end

end
