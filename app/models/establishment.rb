class Establishment < ApplicationRecord
  belongs_to :user
  belongs_to :category
  has_many :reviews, dependent: :destroy
  has_many :images, dependent: :destroy

  validates :title, presence: true
  validates :description, presence: true
  validates :category_id, presence: true
  validates :agreement, presence: true
  validates :picture, presence: true

  paginates_per 20

  mount_uploader :picture, ImageUploader

  def quality_food_average
    average = self.reviews.average(:quality_food)
    return average_round(average)
  end

  def service_quality_average
    average = self.reviews.average(:service_quality)
    return average_round(average)
  end

  def interior_average
    average = self.reviews.average(:interior)
    return average_round(average)
  end

  def owerall_average
    average = (quality_food_average + service_quality_average + interior_average) / 3
    return average_round(average)
  end

  private

  def average_round(average)
    if average.nil?
      return 0
    else
      return average.round(1)
    end
  end
end
