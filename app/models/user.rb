class User < ApplicationRecord
  has_many :establishments, dependent: :destroy
  has_many :images, dependent: :destroy

  devise :database_authenticatable, :registerable,
         :validatable

  validates :username, presence: true


  def active_for_authentication?
    super && self.is_active? # i.e. super && self.is_active
  end

  def is_active?
    self.active_status?
  end

private

end
