class Image < ApplicationRecord
  belongs_to :user
  belongs_to :establishment

  mount_uploader :image, ImageUploader

  validates :image, presence: true
end
