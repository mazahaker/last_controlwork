class ReviewsController < ApplicationController
  def create
    @establishment = Establishment.find(params[:establishment_id])
    @review = @establishment.reviews.build(review_params)
    @review.user_id = current_user.id
    @review.save
    redirect_back(fallback_location: root_path)
  end

  def destroy
    @review = Review.find(params[:id])
    @review.destroy
    redirect_back(fallback_location: root_path)
  end

  private
  def review_params
    params.require(:review).permit(:body, :quality_food, :service_quality, :interior)
  end
end
