class CategoriesController < ApplicationController
  def show
    @category = Category.find(params[:id])
    @establishments = @category.establishments.where(active_status: true).page(params[:page])
  end
end
