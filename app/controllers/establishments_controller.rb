class EstablishmentsController < ApplicationController
  def index
    @categories = Category.all
    if params[:text] != ''
      @establishments = Establishment.where("title LIKE ? or description LIKE ?", "%#{params[:text]}%", "%#{params[:text]}%").where(active_status: true).order('updated_at DESC').page(params[:page])
    else
      @establishments = Establishment.where(active_status: true).order('updated_at DESC').page(params[:page])
    end
  end

  def show
    @establishment = Establishment.find(params[:id])
    redirect_to root_path unless @establishment.active_status?
    @images = @establishment.images
    @reviews = @establishment.reviews.order('updated_at DESC')
    @review_availability = @reviews.find_by(user_id: "#{current_user.id}") if user_signed_in?
  end

  def new
    authenticate_user!
    @establishment = Establishment.new
  end

  def create
    @establishment = Establishment.new(establishment_params)

    if @establishment.save
      redirect_to root_path, notice: 'The establishment will be added after verification'
    else
      render :new
    end

  end

  private

  def establishment_params
    params.require(:establishment).permit(:title, :description, :agreement, :category_id, :picture).merge(user_id: current_user.id)
  end
end
