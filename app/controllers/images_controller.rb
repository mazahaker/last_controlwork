class ImagesController < ApplicationController
  def create
    @establishment = Establishment.find(params[:establishment_id])
    @image = @establishment.images.build(image_params)
    if @image.save
      redirect_to root_path, notice: 'Yeah!'
    else
      redirect_to root_path, notice: 'NOOOO!'
    end
  end

  private

  def image_params
    params.require(:image).permit(:image).merge(user_id: current_user.id)
  end
end
